# Chouette Client

##  Configuration

Chouette client can call [Chouette](https://chouette.enroute.mobi) import API.

You need to have 2 parameters :

* workbench identifier
* a token to authenticate our API requests

To display and edit them, select in the menu Parameters > Security :

![Display parameters](images/chouette_token_api.png)

## Install

You can:

* [Download](https://bitbucket.org/enroute-mobi/chouette-client/downloads/) a binary version of chouette-client for various platforms
* Retrieve and compile locally: `go get bitbucket.org/enroute-mobi/chouette-client`

## Getting started

Display help on Chouette Client :

``` shell
$ chouette-client create --help

Create a Chouette resource

Examples:
  # Create an Document
  chouette-client create document --name=Test --document-type=test /path/to/file

  # Create an Import
  chouette-client create import --name=Test /path/to/file

Usage:
  chouette-client create [command]

Available Commands:
  document    A brief description of your command
  documents   Create Documents with specified files
  import      Create a Import with specified file
  imports     Create Imports with specified files

Flags:
  -h, --help   help for create

Global Flags:
      --base-url string    The address and port of the Chouette API server
      --config string      config file (default is $HOME/.chouette-client.yaml)
      --debug              Enable debug messages
      --workbench string   The Chouette workbench identifier associated to the API requests (required)

Use "chouette-client create [command] --help" for more information about a command.
```

Parameters for Chouette API are described in [Postman Documentation](https://documenter.getpostman.com/view/9950294/Szf3aqMh)

## License

Copyright 2017-2024 enRoute

Licensed under the [Apache License, Version 2.0](./LICENSE) (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions limitations under the License.
