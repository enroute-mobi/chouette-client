package client

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

type Document struct {
	File string `json:"-"`
	Name string
	DocumentType string
	CodeDefinition string
	ID int

	Code *Code
}

func (document *Document) Validate() error {
	if document.File == "" {
		return fmt.Errorf("No documented file specified")
	}

	if _, err := os.Stat(document.File); os.IsNotExist(err) {
		return fmt.Errorf("Specified file doesn't exist: '%s'", document.File)
	}

	if document.Name == "" {
		return fmt.Errorf("No document name specified")
	}

	if document.DocumentType == "" {
		return fmt.Errorf("No document type specified")
	}

	err := document.ParseCodeDefinition()
	if err != nil {
		return err
	}

	return nil
}

func (document *Document) ParseCodeDefinition() error {
	if document.CodeDefinition == "" {
		return nil
	}

	parts := strings.Split(document.CodeDefinition,":")

	if len(parts) != 2 {
		return fmt.Errorf("Invalid code definition")
	}

	document.Code = &Code{
		CodeSpace: parts[0],
		Value: parts[1],
	}

	return nil
}

type Documents struct {
	Files []string
	DocumentType string
	Prefix string
}

func (documents *Documents) Validate() error {
	if len(documents.Files) == 0 {
		return fmt.Errorf("No document file specified")
	}

	for _, file := range documents.Files {
		if _, err := os.Stat(file); os.IsNotExist(err) {
			return fmt.Errorf("Specified file doesn't exist: '%s'", file)
		}
	}

	if documents.DocumentType == "" {
		return fmt.Errorf("No document type specified")
	}

	return nil
}

func (documents *Documents) name(file string) string {
	baseName := filepath.Base(file)
	if documents.Prefix == "" {
		return baseName
	}

	return fmt.Sprintf("%s %s", documents.Prefix, baseName)
}

func (documents *Documents) createDocument(file string) *Document {
	return &Document {
		File: file,
		Name: documents.name(file),
		DocumentType: documents.DocumentType,
	}
}

func (documents *Documents) Documents() []*Document {
	slice := make([]*Document, 0)

	for _, file := range documents.Files {
		slice = append(slice, documents.createDocument(file))
	}

	return slice
}
