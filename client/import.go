package client

import (
	"fmt"
	"os"
	"path/filepath"
)

type Import struct {
	File string `json:"-"`
	Name string
	ArchiveOnFail bool
	AutomaticMerge bool
	FlagUrgent bool

	ID int
}

func (operation *Import) Validate() error {
	if operation.File == "" {
		return fmt.Errorf("No imported file specified")
	}

	if _, err := os.Stat(operation.File); os.IsNotExist(err) {
		return fmt.Errorf("Specified file doesn't exist: '%s'", operation.File)
	}

	if operation.Name == "" {
		return fmt.Errorf("No import name specified")
	}

	return nil
}

type Imports struct {
	Files []string
	Prefix string

	ArchiveOnFail bool
	AutomaticMerge bool
	FlagUrgent bool
}

func (imports *Imports) Validate() error {
	if len(imports.Files) == 0 {
		return fmt.Errorf("No imported file specified")
	}

	for _, file := range imports.Files {
		if _, err := os.Stat(file); os.IsNotExist(err) {
			return fmt.Errorf("Specified file doesn't exist: '%s'", file)
		}
	}

	return nil
}

func (imports *Imports) name(file string) string {
	baseName := filepath.Base(file)
	if imports.Prefix == "" {
		return baseName
	}

	return fmt.Sprintf("%s %s", imports.Prefix, baseName)
}


func (imports *Imports) createImport(file string) *Import {
	return &Import {
		File: file,
		Name: imports.name(file),
		ArchiveOnFail: imports.ArchiveOnFail,
		AutomaticMerge: imports.AutomaticMerge,
		FlagUrgent: imports.FlagUrgent,
	}
}

func (imports *Imports) Imports() []*Import {
	slice := make([]*Import, 0)

	for _, file := range imports.Files {
		slice = append(slice, imports.createImport(file))
	}

	return slice
}
