package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

type Chouette struct {
	BaseURL string
	Workbench string
	Token string
	Debug bool
}

func NewChouette() (*Chouette, error) {
	chouette := &Chouette{
		BaseURL: viper.GetString("base-url"),
		Workbench: viper.GetString("workbench"),
		Token: viper.GetString("token"),
		Debug: viper.GetBool("debug"),
	}

	err := chouette.Validate()
	if err != nil {
		return nil, err
	}

	if (chouette.Debug) {
		fmt.Printf("Chouette config: base-url: '%s' workbench: '%s'\n", chouette.BaseURL, chouette.Workbench)
	}

	return chouette, nil
}

func (chouette *Chouette) Validate() error {
	if chouette.BaseURL == "" {
		return fmt.Errorf("No Chouette base url defined")
	}
	if chouette.Token == "" {
		return fmt.Errorf("No Chouette token defined")
	}
	if chouette.Workbench == "" {
		return fmt.Errorf("No Chouette workbench defined")
	}
	return nil
}

func (chouette *Chouette) Url(resources string) string {
	return fmt.Sprintf("%s/api/v1/workbenches/%s/%s.json", chouette.BaseURL, chouette.Workbench, resources)
}

func (chouette *Chouette) Request(resources string, payload *bytes.Buffer, contentType string) ([]byte, error) {
	client := &http.Client{}
	request, err := http.NewRequest("POST", chouette.Url(resources), payload)

	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", contentType)
	request.Header.Set("Authorization", "Token token=" + chouette.Token)

	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if (chouette.Debug) {
		fmt.Println(string(body))
	}

	if response.StatusCode != 201 {
		// TODO: manage 400 Bad Request JSON respose
		// {"status":"error","message":"La validation a échoué : Document type doit exister, Type de Document doit être rempli(e)"}

		return nil, fmt.Errorf("Request not successful: %d '%s'", response.StatusCode, http.StatusText(response.StatusCode))
	}

	return body, nil
}

func (chouette *Chouette) CreateImport(operation *Import) (error) {
	if (chouette.Debug) {
		fmt.Println(operation)
	}

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)

	err := writer.WriteField("workbench_import[name]", operation.Name)
	if err != nil {
		return err
	}

	file, err := os.Open(operation.File)
	defer file.Close()

	filePart, err := writer.CreateFormFile("workbench_import[file]", filepath.Base(operation.File))
	_, err = io.Copy(filePart, file)

	if err != nil {
		return err
	}

	err = writer.WriteField("workbench_import[options][automatic_merge]", strconv.FormatBool(operation.AutomaticMerge))
	if err != nil {
		return err
	}

	err = writer.WriteField("workbench_import[options][flag_urgent]", strconv.FormatBool(operation.FlagUrgent))
	if err != nil {
		return err
	}

	err = writer.WriteField("workbench_import[options][archive_on_fail]", strconv.FormatBool(operation.ArchiveOnFail))
	if err != nil {
		return err
	}
	err = writer.Close()

	if err != nil {
		return err
	}

	body, err := chouette.Request("imports", payload, writer.FormDataContentType())
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &operation)

	if err != nil {
		return err
	}

	fmt.Println("Import", operation.ID, fmt.Sprintf("'%s'", operation.Name), "created for", operation.File)

	return nil
}

func (chouette *Chouette) CreateDocument(document *Document) (error) {
	if (chouette.Debug) {
		fmt.Println(document)
	}

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)

	err := writer.WriteField("document[name]", document.Name)
	if err != nil {
		return err
	}

	file, err := os.Open(document.File)
	defer file.Close()

	filePart, err := writer.CreateFormFile("document[file]", filepath.Base(document.File))
	_, err = io.Copy(filePart, file)

	if err != nil {
		return err
	}

	err = writer.WriteField("document[document_type]", document.DocumentType)
	if err != nil {
		return err
	}

	if code := document.Code ; code != nil {
		err = writer.WriteField("document[codes][0][code_space]", code.CodeSpace)
		if err != nil {
			return err
		}

		err = writer.WriteField("document[codes][0][value]", code.Value)
		if err != nil {
			return err
		}
	}

	err = writer.Close()

	if err != nil {
		return err
	}

	body, err := chouette.Request("documents", payload, writer.FormDataContentType())
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &document)

	if err != nil {
		return err
	}

	fmt.Println("Document", document.ID, fmt.Sprintf("'%s'", document.Name), "created for", document.File)

	return nil
}

func (chouette *Chouette) CreateDocuments(documents *Documents) (error) {
	if (chouette.Debug) {
		fmt.Println(documents)
	}

	for _, document := range documents.Documents() {
		err := chouette.CreateDocument(document)
		if err != nil {
			return err
		}
	}

	return nil
}

func (chouette *Chouette) CreateImports(imports *Imports) (error) {
	if (chouette.Debug) {
		fmt.Println(imports)
	}

	for _, operation := range imports.Imports() {
		err := chouette.CreateImport(operation)
		if err != nil {
			return err
		}
	}

	return nil
}
