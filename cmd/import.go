package cmd

import (
	"github.com/spf13/cobra"
	"bitbucket.org/enroute-mobi/chouette-client/client"
)

var importOperation client.Import

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import",
	Short: "Create a Import with specified file",
	Long: `Create a Import with specified file

Examples:
  # Create an Import
  chouette-client create import /path/to/file`,
	Args: cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		importOperation.File = args[0]

		err := importOperation.Validate()
		if err != nil {
			return err
		}

		chouette, err := client.NewChouette()
		if (err != nil) {
			return err
		}

		return chouette.CreateImport(&importOperation)
	},
}

func init() {
	createCmd.AddCommand(importCmd)

	importCmd.Flags().StringVar(&importOperation.Name, "name", "", "The name of the created Import (required)")
	importCmd.MarkFlagRequired("name")

	importCmd.Flags().BoolVarP(&importOperation.ArchiveOnFail, "archive-on-fail", "", false, "Archive imported datas if the import failed")
	importCmd.Flags().BoolVarP(&importOperation.AutomaticMerge, "automatic-merge", "", false, "Merge imported dataset (on success)")
	importCmd.Flags().BoolVarP(&importOperation.FlagUrgent, "flag-urgent", "", false, "Flag imported dataset as urgent")
}
