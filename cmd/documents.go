package cmd

import (
	"github.com/spf13/cobra"
	"bitbucket.org/enroute-mobi/chouette-client/client"
)

var documents client.Documents

// documentsCmd represents the documents command
var documentsCmd = &cobra.Command{
	Use:   "documents",
	Short: "Create Documents with specified files",
	Long: `Create Documents with specified files

Examples:
  # Create several Documents
  chouette-client create documents --document-type=test /path/to/*.pdf`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		documents.Files = args

		err := documents.Validate()
		if err != nil {
			return err
		}

		chouette, err := client.NewChouette()
		if (err != nil) {
			return err
		}

		return chouette.CreateDocuments(&documents)
	},
}

func init() {
	createCmd.AddCommand(documentsCmd)

	documentsCmd.Flags().StringVar(&documents.DocumentType, "document-type", "", "The Document Type short name for the created Documents (required)")
	documentsCmd.MarkFlagRequired("document-type")

	documentsCmd.Flags().StringVar(&documents.Prefix, "prefix", "", "The prefix added to the Document name before the file basename")
}
