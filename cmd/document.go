package cmd

import (
	"github.com/spf13/cobra"
	"bitbucket.org/enroute-mobi/chouette-client/client"
)

var document client.Document

// documentCmd represents the document command
var documentCmd = &cobra.Command{
	Use:   "document",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		document.File = args[0]

		err := document.Validate()
		if err != nil {
			return err
		}

		chouette, err := client.NewChouette()
		if (err != nil) {
			return err
		}

		return chouette.CreateDocument(&document)
	},
}

func init() {
	createCmd.AddCommand(documentCmd)

	documentCmd.Flags().StringVar(&document.Name, "name", "", "The name of the created Document (required)")
	documentCmd.MarkFlagRequired("name")

	documentCmd.Flags().StringVar(&document.DocumentType, "document-type", "", "The Document Type short name for the created Document (required)")
	documentCmd.MarkFlagRequired("document-type")

	documentCmd.Flags().StringVar(&document.CodeDefinition, "code", "", "A code (code_space:code_value) added on the created Document")
}
