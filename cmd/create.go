package cmd

import (
	"github.com/spf13/cobra"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a Chouette resource",
	Long: `Create a Chouette resource

Examples:
  # Create an Document
  chouette-client create document --name=Test --document-type=test /path/to/file

  # Create an Import
  chouette-client create import --name=Test /path/to/file`,
}

func init() {
	rootCmd.AddCommand(createCmd)
}
