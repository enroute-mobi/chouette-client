package cmd

import (
	"github.com/spf13/cobra"
	"bitbucket.org/enroute-mobi/chouette-client/client"
)

var imports client.Imports

// importsCmd represents the imports command
var importsCmd = &cobra.Command{
	Use:   "imports",
	Short: "Create Imports with specified files",
	Long: `Create Imports with specified files

Examples:
  # Create an Import for each specified file
  chouette-client create imports /path/to/file`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		imports.Files = args

		err := imports.Validate()
		if err != nil {
			return err
		}

		chouette, err := client.NewChouette()
		if (err != nil) {
			return err
		}

		return chouette.CreateImports(&imports)
	},
}

func init() {
	createCmd.AddCommand(importsCmd)

	importsCmd.Flags().BoolVarP(&imports.ArchiveOnFail, "archive-on-fail", "", false, "Archive imported datas if the import failed")
	importsCmd.Flags().BoolVarP(&imports.AutomaticMerge, "automatic-merge", "", false, "Merge imported dataset (on success)")
	importsCmd.Flags().BoolVarP(&imports.FlagUrgent, "flag-urgent", "", false, "Flag imported dataset as urgent")

	importsCmd.Flags().StringVar(&imports.Prefix, "prefix", "", "The prefix added to the Import name")
}
